FROM eclipse-temurin:17-jdk-jammy
LABEL authors="Jonny Rimkus <jonny@rimkus.it>"

COPY "up/build/libs/up-*-all.jar" "/usr/local/share/up/"
COPY "up/config.json" "/etc/up/"
ENV CONFIG_FILE="/etc/up/config.json"

ENTRYPOINT java -jar /usr/local/share/up/up-*-all.jar -c "$CONFIG_FILE"
